'use strict'

/**
 * This class is a bit of a misnomer, since its main job is to handle the animations being applied
 * to the underlying canvas element.
 */
class Painter {
  #fps = 60 // target frame rate
  #animations = new Map()

  constructor(canvasID) {
    // get canvas element
    this.canvas = document.getElementById(canvasID)
    this.resize()
    
    // start animation loop
    const mspf = 1000 / this.#fps // milliseconds per frame
    const animations = this.#animations
    const ctx = this.canvas.getContext('2d')
    
    // FIXME: throw this code in a worker so it doesn't slow down the main thread
    let previous
    ;(function frame(timestamp) {
      if (previous === undefined) previous = timestamp
      const elapsed = timestamp - previous
      if (elapsed >= mspf) {
        const frames = Math.floor(elapsed / mspf)
        previous += frames * mspf

        // run missing frames for each active animation 
        animations.forEach((animation) => {
          if (animation.on) {
            for (let i=0; i<frames; i++) {
              // run animation & turn it off if it returns falsy
              if (!animation.fn(ctx)) animation.on = false
            }
          }
        })
      }
      window.requestAnimationFrame(frame)
    })(performance.now())
  }

  resize(w, h) {
    this.canvas.width = w || this.canvas.parentNode.clientWidth
    this.canvas.height = h || this.canvas.parentNode.clientHeight
    return this
  }

  // the animation method should take one param,
  // the context on which to render the animation
  add(name, animation, start = true) {
    this.#animations.set(name, {
      fn: animation,
      on: start
    })
    return this
  }
}
