'use strict'

class Tree {
  constructor (options = {}) {
    this.w = options.w || 500
    this.h = options.h || 500

    this.boundary = [{
      coords: {
        x: options.x || Math.floor(this.w / 2),
        y: options.y || this.h
      },
      angle: options.angle || 0, // in degs, 0 is straight up
      color: options.color || _randint(360),
      width: options.width || 25, // branch thickness at the root
    }]
  }

  get grow() {
    const boundary = this.boundary
    const bounds = {w: this.w, h: this.h}
    const params = {
      // growth params
      grow: {
        speed: 7,   // base growth speed
        rand: 3,    // randomness in growth speed
        angle: 12,  // randomness in growth angle
        color: 6,   // randomness in growth color
        min: 0.6,   // minimum thickness for growth
      },
      // branching params
      branch: {
        chance: 12, // chance that a node will branch
        angle: 45,  // randomness in branch angle
        width: 0.4, // width of new branch (proportion of trunk)
        factor: 2,  // how fast branching scales up (higher means faster)
      }
    }
    
    return (context) => {
      // if there are no boundary nodes, there are no frames left to render
      if (boundary.length === 0) return false

      // grow the tree
      for (let i = 0; i < boundary.length; i++) {
        const node = boundary.shift()
        // prune branches that are out of bounds or too thin
        if ((node.coords.x < 0) || (node.coords.x > bounds.w) ||
            (node.coords.y < 0) || (node.coords.y > bounds.h) ||
            (node.width < params.grow.min)) {
          continue
        }

        // find the coords of the new growth
        const dist = params.grow.speed + _randint(params.grow.rand, -params.grow.rand)
        const radAngle = Math.PI * (node.angle / 180)
        const growth = {
          x: node.coords.x + (Math.sin(radAngle) * dist),
          y: node.coords.y - (Math.cos(radAngle) * dist)
        }
        // draw the new branch (line from old boundary node to new)
        const branch = new Path2D
        branch.moveTo(node.coords.x, node.coords.y)
        branch.lineTo(growth.x, growth.y)
        context.lineCap = "round"
        context.lineWidth = node.width
        context.strokeStyle = `hsl(${node.color}, 60%, 40%)`
        context.stroke(branch)

        // check if we're branching off; if so generate a new node, scaling odds of branching up as
        // the node gets thinner
        let trunkWidth = node.width
        const odds = params.branch.chance * ((params.branch.factor / (node.width)) + 1)
        if (_randbool(odds)) {
          const width = node.width * params.branch.width
          trunkWidth -= (width / 2) // only remove half the width from the trunk
          boundary.push({
            coords: growth,
            angle: node.angle + _randint(params.branch.angle, -params.branch.angle),
            color: node.color + _randint(params.grow.color, -params.grow.color),
            width: width
          })
        }
        // add the new boundary to the array
        boundary.push({
          coords: growth,
          angle: node.angle + _randint(params.grow.angle, -params.grow.angle),
          color: node.color + _randint(params.grow.color, -params.grow.color),
          width: trunkWidth
        })
      }

      // if we get to the end, we have more frames to render
      return true
    }
  }
}
