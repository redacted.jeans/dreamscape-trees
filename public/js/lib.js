'use strict'

// FIXME: is this biased?
function _randint(max, min = 0) {
  return Math.floor(Math.random() * (max - min + 1)) + min
}

// FIXME: is this biased?
function _randbool(chance = 50) {
  return Math.random() <= (chance / 100)
}
