Trees
===

Pretty procedurally-generated trees(ish) drawn using the HTML Canvas API.

## Gallery
Some examples generated with the default settings:

![A green/blue tree that leans somewhat to the left.](/examples/ex1.png)

![A low purple tree that diverges into two main branches.](/examples/ex2.png)

![A tall blue that extends nearly to the top of the screen.](/examples/ex3.png)

![A small reddish-orange tree that leans to the right.](/examples/ex4.png)

## Credits
Inspired in part by [inconvergent's article about trees](https://inconvergent.net/generative/trees).
